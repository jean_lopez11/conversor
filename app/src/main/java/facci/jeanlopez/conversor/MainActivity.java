package facci.jeanlopez.conversor;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void obtenerValorC(View view){

        EditText valorFaren = findViewById(R.id.itxt_f);
        float valorFf = Float.parseFloat(valorFaren.getText().toString());
        double valorF = (valorFf-32)/1.8;
        TextView mensajeFinal = findViewById(R.id.txt_resultC);
        mensajeFinal.setText(String.valueOf(valorF));
    }


    public void obtenerValorF(View view){

        EditText valorCelcius = findViewById(R.id.itxt_c);
        float valorCelciusf = Float.parseFloat(valorCelcius.getText().toString());
        double valorF = (valorCelciusf*1.8)+32;
        TextView mensajeFinal = findViewById(R.id.txt_resultF);
        mensajeFinal.setText(String.valueOf(valorF));
    }
}